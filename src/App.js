import React, { Component } from 'react';
import './App.css';
import ProjectTable from './ProjectTable.js';
import ProjectList from './ProjectList.json';	

class App extends Component {
  render() {
    return (
      <div className="App">
        <ProjectTable  projects = {ProjectList}/>
      </div>
    );
  }
}

export default App;
