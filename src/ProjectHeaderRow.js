import React from 'react';

class ProjectHeaderRow extends React.Component {
	render(){
		const headers = this.props.headers;
		const headerDetails = Array.from(headers.values()).map((head, index) =>
			<th key = {index}>{head}</th>);	
		return (<tr>{headerDetails}</tr>);
	}
}

export default ProjectHeaderRow;