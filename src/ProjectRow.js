import React from 'react';

class ProjectRow extends React.Component {
	render(){
		const project = this.props.project;
		const projectArray = Object.values(project);
		const rowDetails = projectArray.map((item, index) =>
								 <td key = {index}>{item}</td>)		
		return (<tr>{rowDetails}</tr>);
	}
}

export default ProjectRow;