import React from 'react';

import ProjectRow from './ProjectRow.js';
import ProjectHeaderRow from './ProjectHeaderRow.js';

class ProjectTable extends React.Component {
	render(props) {
				const projects = this.props.projects;
				const headers = new Set();
				for(var i= 0; i < projects.length; i++){
						var keyList = Object.keys(projects[i]);
						for(var j = 0; j < keyList.length; j++){
							headers.add(keyList[j]);
							}
					}
				const headerRow = <ProjectHeaderRow headers = {headers} />;
				const rows = projects.map((projectEl, index) =>
					<ProjectRow key = {index} project = {projectEl} />);
					
				return(
							<table>
								<thead>{headerRow}</thead>
								<tbody>{rows}</tbody>
							</table>
				);
			
	}
}

export default ProjectTable;